#!/usr/bin/env python3


import cmath
import csv
import numpy as np
import os
import subprocess
import time
import pandas as pd
import plotly.graph_objects as go
import plotly.io as pio
import plotly.express as px
import pyautogui
import random
import re

colors = "aliceblue,antiquewhite,aqua,aquamarine,azure,beige,bisque,black,blanchedalmond,blue,blueviolet,brown,burlywood,cadetblue,chartreuse,chocolate,coral,cornflowerblue,cornsilk,crimson,cyan,darkblue,darkcyan,darkgoldenrod,darkgray,darkgrey,darkgreen,darkkhaki,darkmagenta,darkolivegreen,darkorange,darkorchid,darkred,darksalmon,darkseagreen,darkslateblue,darkslategray,darkslategrey,darkturquoise,darkviolet,deeppink,deepskyblue,dimgray,dimgrey,dodgerblue,firebrick,floralwhite,forestgreen,fuchsia,gainsboro,ghostwhite,gold,goldenrod,gray,grey,green,greenyellow,honeydew,hotpink,indianred,indigo,ivory,khaki,lavender,lavenderblush,lawngreen,lemonchiffon,lightblue,lightcoral,lightcyan,lightgoldenrodyellow,lightgray,lightgrey,lightgreen,lightpink,lightsalmon,lightseagreen,lightskyblue,lightslategray,lightslategrey,lightsteelblue,lightyellow,lime,limegreen,linen,magenta,maroon,mediumaquamarine,mediumblue,mediumorchid,mediumpurple,mediumseagreen,mediumslateblue,mediumspringgreen,mediumturquoise,mediumvioletred,midnightblue,mintcream,mistyrose,moccasin,navajowhite,navy,oldlace,olive,olivedrab,orange,orangered,orchid,palegoldenrod,palegreen,paleturquoise,palevioletred,papayawhip,peachpuff,peru,pink,plum,powderblue,purple,red,rosybrown,royalblue,rebeccapurple,saddlebrown,salmon,sandybrown,seagreen,seashell,sienna,silver,skyblue,slateblue,slategray,slategrey,snow,springgreen,steelblue,tan,teal,thistle,tomato,turquoise,violet,wheat,white,whitesmoke,yellow,yellowgreen"
colors = "blue,pink,red,green,yellow,gray,purple"


def remove_outliers(arr, times_sd=1):
    """Remove outliers from 'times_sd' tails. Works on arrays of numbers."""
    elements = np.array(arr)
    mean = np.mean(elements)
    sd = np.std(elements)
    final_list = [x for x in arr if (x > mean - times_sd * sd)]
    final_list = [x for x in final_list if (x < mean + times_sd * sd)]
    return final_list


def plot_line(data, plot_name="CHART", x_axis="X", y_axis="Y", image_name="img"):
    """plots a simple array. X axis is simply a counter of the size of the array
    """
    counter = 0
    fig = go.Figure()
    print(data)
    num_samples = data.__len__()
    x = np.arange(num_samples)
    fig.add_trace(go.Scatter(x=x, y=data, mode='lines', name='lines'))
    # fig.update_yaxes(range=[0, max_y])
    counter = counter + 1
    fig.update_layout(
        title=plot_name,
        xaxis_title=x_axis,
        yaxis_title=y_axis)

    fig_path = "images/{}.png".format(image_name)
    fig.write_image(fig_path)


def plot_lines(dataset, names, plot_name="CHART", x_axis="X", y_axis="Y", image_name="img"):
    """plots datasets, that is, xy data (two columns
    """
    counter = 0
    fig = go.Figure()
    for data in dataset:
        print(data)
        num_samples = data.__len__()
        x = np.arange(num_samples)
        print(names, counter)
        line_name = names[counter]

        fig.add_trace(go.Scatter(x=x, y=data, mode='lines', name=line_name))
        # fig.update_yaxes(range=[0, max_y])
        counter = counter + 1
    fig.update_layout(
        title=plot_name,
        xaxis_title=x_axis,
        yaxis_title=y_axis)

    fig_path = "images/{}.png".format(image_name)
    fig.write_image(fig_path)


def get_experiment_names(dir="."):
    """

    :param dir: the root directory containing csv files
    :return: a set() with the names of experiments
    """
    experiments = set()
    experiments = []
    ld = os.listdir(dir)
    ld = sorted(ld)
    for file in ld:
        filename = os.fsdecode(file)
        if filename.endswith(".csv"):
            experiment_name = filename.split("-")
            file = csv.reader(open(dir + filename, newline=''), delimiter=',', quotechar='"')
            for line in file:
                if line[0] != "capacity":
                    experiment_name.append(line[4])
                    experiment_name.append(line[6])
                    experiment_name.append(line[8])
                    experiment_name.append(line[10])
            experiments.append(experiment_name)
    return experiments


def create_dataframe(get_experiment_names):
    """Input get_experiment_names
    :return:
    """
    # ['20200323', '001', '90', '1', '100', '1', 'results.csv', 'NaN', '35963.6', '0.0', '0.0']
    new_data = []
    for data in get_experiment_names:
        try:
            print("DATA", data)
            app = float("{}.{:0>1}".format(int(data[2]), int(data[3])))
            app = app * 10
            instance = float("{}.{:0>1}".format(int(data[4]), int(data[5])))
            instance = instance * 10
            delay = float(data[7])
            delay = round(delay)
            # if delay > 0:
            line = [int(data[0]), int(data[1]), app, instance, delay, float(data[8]), float(data[9]),
                    float(data[10])]
            print("LINE", line)
            new_data.append(line)
        except:
            pass
    columns = ["date", "num", "app", "instance", "avg_wait", "avg_req", "met_req", "percent_req"]
    df = pd.DataFrame(new_data, columns=columns)
    df.to_csv("df.csv")
    return df


def plot_3d(filename, image_name, df=px.data.iris(), x_axis='sepal_length',
            y_axis='sepal_width', z_axis='petal_width',
            group_color='petal_length', group_symbol='species'):
    print(df)
    fig = px.scatter_3d(df, x=x_axis, y=y_axis, z=z_axis, color=group_color, symbol=group_symbol)
    #filepath = filename.split(".")[0]
    filepath =filename.replace(".csv", "")
    image_name = "{}.{}.png".format(filepath, image_name)
    image_name = image_name.replace(" ", "")
    fig.write_image(image_name)
    #fig.show()


def plot_line_2d(filename, image_name, df=px.data.iris(), x_axis='sepal_length',
                 y_axis='sepal_width', y_range=[]):
    print(df)
    if range:
        fig = px.line(df, x=x_axis, y=y_axis, range_y=y_range)
    else:
        fig = px.line(df, x=x_axis, y=y_axis)
    filepath = filename.split(".")[0]
    image_name = "{}.{}.png".format(filepath, image_name)
    image_name = image_name.replace(" ", "")
    fig.write_image(image_name)
    # fig.show()


def plot_file(
        file="/home/junior/SSD/Dev/thesis-fog-vanet/VehicleTasks/analysis/20200330-001/20200330-001-90-2-90-2-results.csv",
        x_axis=2, y_axis=4, y_range=[]):
    """Open a csv file from VehicleTasks and plots a columns. Uses the name of the file and the name of the column
    to fill the information in plot.

    :param file:
    :param x_axis: Preferably, use 2. May use 0, 1, 2, 3
    :param y_axis: use 4, 6, 8, 10
    :return:
    """
    df = pd.read_csv(file)
    print(df)
    plot_line_2d(file, "{}.{}".format(df.columns[x_axis], df.columns[y_axis]),
                 df=df, x_axis=df.columns[x_axis], y_axis=df.columns[y_axis], y_range=y_range)


def one_line_files():
    """Plots 3D plot from a directory containing onelined csv files from VehicleTasks
    Ex.: /home/junior/SSD/Dev/thesis-fog-vanet/VehicleTasks/analysis/20200329-001

    :return:
    """
    dir = "/home/junior/SSD/Dev/thesis-fog-vanet/VehicleTasks/analysis/20200330-001/"
    gen = get_experiment_names(dir)
    print(gen)
    cd = create_dataframe(gen)
    print(cd)
    plot_3d(df=cd, x_axis="app", y_axis="instance", z_axis="percent_req", group_color="avg_wait",
            group_symbol="avg_wait")
    # str = "{}.{:0>1}".format(1, 2)
    # print(str)


def plot_each_file():
    """Plots  avg_wait, avg_req, met_req, percent_req


    :return:
    """
    dir = "/home/junior/SSD/Dev/thesis-fog-vanet/VehicleTasks/analysis/20200330-001/"
    experiments = []
    ld = os.listdir(dir)
    ld = sorted(ld)
    columns = [4, 6, 8, 10]
    ranges = {4: 200, 6: 215000, 8: 40000, 10: 1}

    for file in ld:
        if file.endswith(".csv"):
            filepath = os.fsdecode(file)
            print(filepath)
            filepath = dir + filepath
            for c in columns:
                plot_file(file=filepath, x_axis=2, y_axis=c, y_range=[0, ranges[c]])


def create_heterogeneity_files():
    """Deve pegar um ou mais valores de cada linha e criar um dataset. Depois, plotar graficos 3d com esses valores 
    para gerar insights. Cria os arquivos heterogeneity.0.csv na pasta principal
    """
    dir = "/home/junior/SSD/Dev/thesis-fog-vanet/VehicleTasks/analysis/20200330-001/"
    experiments = []
    ld = os.listdir(dir)
    ld = sorted(ld)
    columns = [4, 6, 8, 10]
    ranges = {4: 200, 6: 215000, 8: 40000, 10: 1}
    new_columns = ["app", "instance", "avg_wait", "avg_req", "met_req", "percent_req"]
    df_main = pd.DataFrame(columns=new_columns)
    counter = 0
    for file_row in range(0, 4):
        for file in ld:
            if file.endswith(".csv"):
                row = []
                split_name = file.split("-")
                app = "{}.{}".format(split_name[2], split_name[3])
                instance = "{}.{}".format(split_name[4], split_name[5])
                app = float("{}.{:0>2}".format(split_name[2], split_name[3])) * 100
                instance = float("{}.{:0>2}".format(split_name[4], split_name[5])) * 100
                row.append(app)
                row.append(instance)
                filepath = os.fsdecode(file)
                print(filepath)
                filepath = dir + filepath
                df_file = pd.read_csv(filepath)
                for c in columns:
                    value = df_file.at[file_row, df_file.columns[c]]
                    row.append(value)
                print(row)
                df_main.loc[counter] = row
                counter = counter + 1
        print(df_main)
        df_main.to_csv("heterogeneity.{}.csv".format(file_row))


def plot_heterogeneity_files():
    dir = "/home/junior/SSD/Dev/thesis-fog-vanet/VehicleTasks/analysis/"
    experiments = []
    ld = os.listdir(dir)
    ld = sorted(ld)
    columns = [2, 3, 4, 5]
    new_columns = ["app", "instance", "avg_wait", "avg_req", "met_req", "percent_req"]
    ranges = {4: 200, 6: 215000, 8: 40000, 10: 1}
    for file in ld:
        if file.endswith(".csv") and file.startswith("heterogeneity"):
            for c in columns:
                filepath = os.fsdecode(file)
                print(filepath)
                filepath = dir + filepath
                df = pd.read_csv(filepath)
                print(df)
                axis_y =  new_columns[c]
                plot_3d(filepath, "{}".format(axis_y), df=df, x_axis='app', y_axis='instance', z_axis=axis_y,
                        group_color=axis_y, group_symbol=axis_y)


if __name__ == '__main__':
    # create_heterogeneity_files()
    plot_heterogeneity_files()
    pyautogui.alert('Charts ready', "VEHICLE TASKS")  # always returns "OK"
