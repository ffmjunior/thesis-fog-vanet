library("ggplot2")

setwd("E:/Dev/thesis-fog-vanet/VehicleTasks/build/classes/ext/RScripts")

    font_size <- 40
    line_size <- 2


    plot_name <-'Capacity-load plot'
    
    
    x_axis_label <- "Capacity"
    y_axis_label <- "Application Load"

    pallete <-c('#2d7dd2', '#97cc04', '#eeb902', '#f45d01', '#474647')


barPlot <-function() {

    p<-ggplot(df2, aes(x = dose, y = len, fill = supp)) +
            geom_bar(stat = "identity", color = "black",
            position = position_dodge()) +
            geom_errorbar(aes(ymin = len - sd, ymax = len + sd), width = .2,
            position = position_dodge(.9))
            print(p)
            p + labs(title = "Tooth length per dose", x = "Dose (mg)", y = "Length") +
            theme_classic() +
            scale_fill_manual(values = c('#999999', '#E69F00'))

}

## plots a line chart with error bars
## main line plot

## plots the latency resulting from the capacity-load relationship
## latency appears in the y axis 
## capacity in x axis
## this plot groups data by load
## follow these guidelines to create related plots
## STEPS
## convert values of interest to factor e group data that appears in lines
## replace capacity (X), avg_wait(Y) and load (group) with the values of interest
## on geom_errorbar replace avg_wait(Y) with the values of interest

linePlot_latency <-function() {
    mydata[["load"]] <-as.factor(mydata[["load"]]) # use this to convert values of interest to factor e group data that appears in lines
    x_values <- mydata[["capacity"]] # use this to choose values in x axis
    
    p<-ggplot(mydata, aes(x = factor(capacity), y = avg_wait, group = load, color = load)) + # replace capacity (X), avg_wait(Y) and load (group) with the values of interest
            geom_line(size = line_size) +
            geom_point() +
            geom_errorbar(size = line_size, aes(ymin = avg_wait - avg_wait_sd, ymax = avg_wait + avg_wait_sd), width = .2, position = position_dodge(0.05)) # on geom_errorbar replace avg_wait(Y) with the values of interest

            print(p)
#Finished line plot
            p +
            labs(title = plot_name, x = x_axis_label , y = y_axis_label, cex = 2.0, ps = 10) + # replace the names in axis
            theme_classic(base_size = font_size) +
            geom_hline(yintercept = seq(0, max(mydata[["avg_wait"]]), 5), linetype = "dashed", color = "black") +
            theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
            scale_x_discrete(breaks = x_values) +
            scale_color_manual(values = pallete)
}

## line plot
## requisition percent in y axis
## throughput in x axis
## groups by taskrate

lineplot_req_throughput <-function() {
    mydata[["percent_req"]] <- mydata[["percent_req"]] * 100
    mydata[["percent_req_sd"]] <- mydata[["percent_req_sd"]] * 100 
    mydata[["taskRate"]] <-as.factor(mydata[["taskRate"]])#use this to convert values of interest to factor e group data that appears in lines
    x_values <-mydata[["throughput"]]#use this to choose values in x axis
    #Default line plot
    p<-ggplot(mydata, aes(x = factor(throughput), y = percent_req, group = taskRate, color = taskRate)) +
    geom_line( size=2) +
    geom_point() +
    geom_errorbar(aes(ymin = percent_req - percent_req_sd, ymax = percent_req + percent_req_sd), width = .2, position = position_dodge(0.05))

    print(p)
    #Finished line plot
    p +
    labs(title = plot_name, x = x_axis_label, y = y_axis_label, cex = 2.0, ps = 10) +#replace the names in axis
    theme_classic(base_size = 60) +
    geom_hline(yintercept = seq(0, max(mydata[["percent_req"]]), 5), linetype = "dashed", color = "black") +
    theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
    scale_x_discrete(breaks = mydata[["throughput"]]) +
    scale_color_manual(values = pallete)
}

## plots the latency vs rate 
## 

linePlot_latency_rate <-function() {
    mydata[["load"]] <-as.factor(mydata[["load"]]) # use this to convert values of interest to factor e group data that appears in lines
    x_values <- mydata[["taskrate"]] # use this to choose values in x axis
    
    p<-ggplot(mydata, aes(x = factor(taskRate), y = avg_wait, group = load, color = load)) + # replace capacity (X), avg_wait(Y) and load (group) with the values of interest
            geom_line(size = line_size) +
            geom_point() +
            geom_errorbar(size = line_size, aes(ymin = avg_wait - avg_wait_sd, ymax = avg_wait + avg_wait_sd), width = .2, position = position_dodge(0.05)) # on geom_errorbar replace avg_wait(Y) with the values of interest

            print(p)
#Finished line plot
            p +
            labs(title = plot_name, x = x_axis_label , y = y_axis_label, cex = 2.0, ps = 10) + # replace the names in axis
            theme_classic(base_size = font_size) +
            geom_hline(yintercept = seq(0, max(mydata[["avg_wait"]]), 5), linetype = "dashed", color = "black") +
            theme(axis.text.x = element_text(angle = 90, hjust = 1)) +
            scale_x_discrete(breaks = x_values) +
            scale_color_manual(values = pallete)
}



linePlot_req <-function() {
#Default line plot
    p<-ggplot(mydata, aes(x = capacity, y = avg_req, group = load, color = load)) +
            geom_line() +
            geom_point() +
            geom_errorbar(aes(ymin = avg_req - avq_reg_sd, ymax = avg_req + avq_reg_sd), width = .2, position = position_dodge(0.05))



            print(p)
#Finished line plot
            p + scale_y_continuous(breaks = seq(1, 1000, 5))
            p + labs(title = plot_name, x = "Node Capacity", y = "Total Service Time", cex = 2.0, ps = 10) + theme_classic(base_size = 30) +
            scale_color_manual(values = c('#006400', '#FF0000', '#0000CD', '#FFD700', '#8A2BE2', '#00CED1', '#C71585', '#FF1493', '#DC143C', '#FF8C00')) + scale_y_continuous(breaks = seq(0, max(mydata[["avg_req"]]), max(mydata[["avg_req"]]) / 10)) +
            geom_hline(yintercept = seq(0, max(mydata[["avg_req"]]), max(mydata[["avg_req"]]) / 10), linetype = "dashed", color = "black")
#+ xlim(0,20) # o x é deitado
}

linePlot_percent_req <-function() {
#Default line plot
    p<-ggplot(mydata, aes(x = capacity, y = avg_wait, group = load, color = load)) +
            geom_line() +
            geom_point() +
            geom_errorbar(aes(ymin = percent_req - percent_req_sd, ymax = percent_req + percent_req_sd), width = .2, position = position_dodge(0.05))

            print(p)
#Finished line plot
            p + scale_y_continuous(breaks = seq(1, 1000, 5))
            p + labs(title = plot_name, x = "Node Capacity", y = "Total Service Time (ms)", cex = 2.0, ps = 10) + theme_classic(base_size = 30) +
            scale_color_manual(values = c('#006400', '#FF0000', '#0000CD', '#FFD700', '#8A2BE2', '#00CED1', '#C71585', '#FF1493', '#DC143C', '#FF8C00')) + scale_y_continuous(breaks = seq(0, max(mydata[["avg_wait"]]), 10)) +
            geom_hline(yintercept = seq(0, max(mydata[["avg_wait"]]), max(mydata[["avg_wait"]]) / 10), linetype = "dashed", color = "black")

}

linePlot_percent_tp <-function() {
    #que porra era esse tp?
#Default line plot dados, 
    p<-ggplot(mydata, aes(x = capacity, y = avg_wait, group = load, color = load)) +
            geom_line() +
            geom_point() +
            geom_errorbar(aes(ymin = percent_req - percent_req_sd, ymax = percent_req + percent_req_sd), width = .2, position = position_dodge(0.05))



            print(p)
#Finished line plot x = baixo y = lado esquerdo
            p + scale_y_continuous(breaks = seq(1, 1000, 5))
            p + labs(title = plot_name, x = "Node Capacity", y = "Total Service Time (ms)", cex = 2.0, ps = 10) + theme_classic(base_size = 30) +
            scale_color_manual(values = c('#006400', '#FF0000', '#0000CD', '#FFD700', '#8A2BE2', '#00CED1', '#C71585', '#FF1493', '#DC143C', '#FF8C00')) + scale_y_continuous(breaks = seq(0, max(mydata[["percent_req"]]), max(mydata[["percent_req"]]) / 10)) +
            geom_hline(yintercept = seq(0, max(mydata[["percent_req"]]), max(mydata[["percent_req"]]) / 10), linetype = "dashed", color = "black")
#+ xlim(0,20) # o x é deitado
}



mydata <-read.csv("results.csv")
mydata <-mydata[order(mydata[["load"]], decreasing = TRUE), ]
mydata
time = format(Sys.time(), "%Y %m %d %H %M %S")
image_name = paste0("img_partial ", time, ".png")
#png(file=image_name, width = 1024, height = 1024, units = "px", pointsize = 12, bg = "white",  type = c("cairo", "cairo-png", "Xlib", "quartz"))
png(file = image_name, width = 5000, height = 5000, units = "px", pointsize = 12)

# linePlot_latency()
# lineplot_req_throughput()
linePlot_latency_rate()

dev.off()



print("End")
