setwd("D:/Dev/thesis-fog-vanet/Scripts")

#Load the packages required to read XML files.
library("XML")
library("methods")

#Give the input file name to the function.
result <-xmlParse(file = "index.xml")

#Exract the root node form the xml file.
rootnode <-xmlRoot(result)

#Find number of nodes in the root.
rootsize <-xmlSize(rootnode)

#Print the result.
print(rootsize)

#Print the first result.
sizes <- c()
for (i in c(2 : rootsize)) {
    name <-rootnode[[i]][['name']][1][1]
    size <-rootnode[[i]][['package']][['size']][1][1]
    sizes <- c(sizes, size)
    print(name)
    print(size)
}

print(sizes)
hist(sizes)
