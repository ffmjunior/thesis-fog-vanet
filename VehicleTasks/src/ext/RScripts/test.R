    library("ggplot2")

    setwd("E:/Dev/thesis-fog-vanet/Scripts")

    plot_name <-'Capacity-load plot'
    plot_name <- 'Requests Percent Plot'
    x_axis_label <- "Throughput (Mbps)"
    y_axis_label <- "Requests Met (%)"

    pallete <-c('#2d7dd2', '#97cc04', '#eeb902', '#f45d01', '#474647')

    mydata <-read.csv("results.csv")
    mydata <-mydata[order(mydata[["load"]], decreasing = TRUE), ]
    mydata
    time = format(Sys.time(), "%Y %m %d %H %M %S")
    image_name = paste0("img_partial ", time, ".png")
    png(file = image_name, width = 3000, height = 3000, units = "px", pointsize = 12)

    mydata[["percent_req"]] <- mydata[["percent_req"]] * 100
    mydata[["percent_req_sd"]] <- mydata[["percent_req_sd"]] * 100 
    mydata[["taskRate"]] <-as.factor(mydata[["taskRate"]])#use this to convert values of interest to factor e group data that appears in lines
    x_values <-mydata[["throughput"]]#use this to choose values in x axis
    #Default line plot
    p<-ggplot(mydata, aes(x = factor(throughput), y = percent_req, group = taskRate, color = taskRate)) +
    geom_line( size=2) +
    geom_point() +
    geom_errorbar(aes(ymin = percent_req - percent_req_sd, ymax = percent_req + percent_req_sd), width = .2, position = position_dodge(0.05))

    print(p)
    #Finished line plot
    p +
    labs(title = plot_name, x = x_axis_label, y = y_axis_label, cex = 2.0, ps = 10) +#replace the names in axis
    theme_classic(base_size = 60) +
    geom_hline(yintercept = seq(0, max(mydata[["percent_req"]]), 5), linetype = "dashed", color = "black") +
    theme(axis.text.x = element_text(angle = 45, hjust = 1)) +
    scale_x_discrete(breaks = mydata[["throughput"]]) +
    scale_color_manual(values = pallete)

    dev.off()



    print("End")
