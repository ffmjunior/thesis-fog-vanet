/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

/**
 *
 * @author junior
 */
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

class LogWindow extends JFrame {

    private JTextArea textArea = new JTextArea();

    public LogWindow() {
        super("");
        setSize(300, 300);
        add(new JScrollPane(textArea));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setVisible(true);
        setAlwaysOnTop(true);

    }

    public void cleanScreen() {
        textArea.setText("");
    }

    public void showInfo(String data) {
        textArea.append(data);
        this.validate();
    }
}

class WindowHandler extends Handler {

    private LogWindow window = null;

    private Formatter formatter = null;

    private Level level = null;

    private static WindowHandler handler = null;

    private WindowHandler() {
        LogManager manager = LogManager.getLogManager();
        String className = this.getClass().getName();
        String level = manager.getProperty(className + ".level");
        setLevel(level != null ? Level.parse(level) : Level.INFO);
        if (window == null) {
            window = new LogWindow();
        }
    }

    public static synchronized WindowHandler getInstance() {
        if (handler == null) {
            handler = new WindowHandler();
        }
        return handler;
    }

    public synchronized void publish(LogRecord record) {
        String message = null;
        if (!isLoggable(record)) {
            return;
        }
        //message = getFormatter().format(record);
        message = record.getMessage();
        window.showInfo(message);
    }

    public synchronized void limpar() {
        window.cleanScreen();
    }

    public void close() {
    }

    public void flush() {
    }
}

public class Monitor {

    private WindowHandler handler = null;

    private Logger logger = null;

    private Date date;

    public Monitor() {

        date = new Date();

        handler = WindowHandler.getInstance();

        //logger = Logger.getLogger("logging.handler");
        //System.out.println("Package: " + Monitor.class.getPackage());
        logger = Logger.getLogger("Monitor");
        //System.out.println(logger.getName());
        logger.addHandler(handler);
        logger.info("MIDDUCA MONITOR!\n");
    }

    public void logMessage(String msg) {
        logger.info(date + "\n" + msg + "\n");
    }

    public void logMessage(String msg, boolean clean) {
        if (clean) {
            handler.limpar();
        }
        logger.info(date + "\n" + msg + "\n");
    }

    /*
    public static void main(String args[]) {
        System.out.println("Criando monitor");
        Monitor demo = new Monitor();
        System.out.println("Executando método");
        demo.logMessage();
        demo.logMessage("Só mais uma!");
        
    }
     */
}
