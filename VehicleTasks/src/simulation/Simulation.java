/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simulation;

import fogTasks.Application;
import fogTasks.ApplicationInstance;
import fogTasks.FogNode;
import results.simulation.BasicSimulationResult;
import java.io.IOException;
import java.net.URL;
import util.RequestQueue;
import util.Statistics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import results.experiment.BasicExperimentResult;
import results.experiment.ExperimentResult;
import results.simulation.SimulationResult;
import util.ApplicationStorage;
import util.FileTools;
import util.ResultFile;

/**
 * The core of this tool. Runs simulations from inside a fog node. To date
 * contains elements related to fogNode, schedulers, external files, simulation
 * parameters, network parameters, application instance parameters, and metrics.
 * To add more metrics: > Create the parameters as atributes in the class. >
 * Store the measures in in ArrayList, whether Double or Integer. Preferably
 * create those as atributes of the class. Remember to clear the data after the
 * executions when applies. > At the end of each iteration, in
 * BasicSimulationResult simulation, add the result to the SimulationResult
 * using decorator pattern. > After each iteration, in the method run, inside
 * the innermost "for" loop, use a previously created arraylist to store data
 * from your metric after each iteration arount "simulationResults". > Use class
 * {@link Statistics} to perform que proper calculations to your arrayList. >
 * Add the result you want to the BasicExperimentResult using decorator pattern.
 * > Add the name of your metric to the header of the resultFile in the main
 * constructor.
 *
 *
 *
 * @author junior
 */
public class Simulation {

    // Simulation Name
    String name;

    // Related to fog Node
    private FogNode fogNode;
    private RequestQueue appQueue;
    private ApplicationStorage appStorage;

    // Schedulers
    Map<Integer, Application> applicationScheduler;

    // External files
    FileTools rFile;
    ResultFile resultFile;
    URL scriptsUrl;

    // simulation parameters    
    private int repetitions;
    private int simulationDuration;
    private ArrayList<Integer> taskGenerationIntervals;
    private ArrayList<Integer> nodeCapacities;
    private ArrayList<Integer> applicationLoads;
    private ArrayList<Double> throughputs;

    // network parameters
    double networkThroughput = 10; // mbps

    // application parameters
    
    double applicationSizeMean = 2;
    double applicationSizeDeviation = 1;
    
    // application id
    double applicationIdMean = 10;
    double applicationIdDeviation = 5;

    // application instance parameters
    double instanceIdMean = 10;
    double instanceIdDeviation = 1;

    // Metrics
    private ArrayList<Double> waitingTimes;
    private ArrayList<Double> totalLatencies;
    private int countRequests;
    private int countMetRequests;
    private int metApplicationRequestRatio;

    /**
     * The default constructor initializes lists regarding parameters and
     * metrics.
     */
    public Simulation() {
        // this(30, 60 * 60 * 1000, new Integer[]{180}, new Integer[]{50}, new Integer[]{5});
        this.taskGenerationIntervals = new ArrayList<>();
        this.nodeCapacities = new ArrayList<>();
        this.applicationLoads = new ArrayList<>();
        this.applicationLoads = new ArrayList<>();
        this.throughputs = new ArrayList<>();
        applicationScheduler = new HashMap<>();

               
    }

    public Simulation(String name) {
        this();
        this.name = name;
        try {
            scriptsUrl = getClass().getResource("../ext/RScripts/");
            System.out.println("RESULT FILE " + scriptsUrl.getFile());

        } catch (Exception e) {
            System.out.println("CAN'T LOAD RESULT FILE!");
        }

        resultFile = new ResultFile(
                scriptsUrl.getPath() + this.name + "results.csv",
                "capacity, load, taskRate, throughput, avg_wait, avg_wait_sd, avg_req, avq_reg_sd, met_req, met_req_sd, percent_req, percent_req_sd,",
                false);

//        rFile = new FileTools(scriptsUrl.getPath() + "plot.R");
//        rFile.replace("setwd", "setwd(\"E:/Dev/thesis-fog-vanet/Scripts\")"); 
    }

    /**
     * The main constructor sets values to parameters. Requires replacement with
     * creational patterns.
     *
     * @param repetitions
     * @param simulationDuration
     * @param taskGenerationInterval
     * @param capacities
     * @param loads
     */
    public Simulation(int repetitions, int simulationDuration, Integer[] taskGenerationInterval, Integer[] capacities, Integer[] loads) {
        this();
        this.repetitions = repetitions;
        this.simulationDuration = simulationDuration;
        this.setTaskGenerationIntervals(taskGenerationInterval);
        this.setNodeCapacities(capacities);
        this.setApplicationLoads(loads);

    }

    /**
     * Determines de task creation rate. Internally, uses a random number
     * generator to calculate de probability of tue creation of a new task in
     * that tick. The probability of the creation of a task in that tick is
     * 1/timeDifference. The result represents an exponential distribution with
     * mean equals timeDifference.
     *
     * @param timeDifferente The main parameter of the probability distribution.
     * @return True when the distribution indicates the creation of a task.
     */
    public boolean newTask(int timeDifferente) {
        Random r = new Random();
        int taskSelection = r.nextInt(timeDifferente) + 1; // one task at 500 ms on average
        if (taskSelection == timeDifferente) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Schedules applications so the FogNode can download them to respond to
     * requisitions. Internally, fills the
     * {@link Simulation#applicationScheduler}. Depending on the parameters set
     * on the constructor or using setters.
     */
    public void scheduleApplictions() {
        applicationScheduler.clear();
        int maxTime = 0;
        Application a;
        double applicationSize;
        double applicationId;
        while (maxTime < simulationDuration) {
            applicationSize = Statistics.getPositiveNormalDistributionSamples(applicationSizeMean, applicationSizeDeviation);
            applicationId = Statistics.getPositiveNormalDistributionSamples(applicationIdMean, applicationIdDeviation);
            a = new Application(applicationSize, (int) applicationId);
            maxTime = maxTime + this.downloadTime(a);
            applicationScheduler.put(maxTime, a);
//            System.out.println("Max time " + maxTime + " Application size " + applicationSize + " Id " + a.getApplicationID());
        }
    }

    /**
     * Creates a set of initial applications inside the FogNode. Not yet
     * implemented.
     */
    public void createInitialApplicationSet() {
        applicationScheduler.clear();
        int maxTime = 0;
        Application a;
        double applicationSize;
        double applicationId;
        while (maxTime < simulationDuration) {
            applicationSize = Statistics.getPositiveNormalDistributionSamples(applicationSizeMean, applicationSizeDeviation);
            applicationId = Statistics.getPositiveNormalDistributionSamples(applicationIdMean, applicationIdDeviation);
            a = new Application(applicationSize, (int) applicationId);
            maxTime = maxTime + this.downloadTime(a);
            applicationScheduler.put(maxTime, a);
//            System.out.println("Max time " + maxTime + " Application size " + applicationSize + " Id " + a.getApplicationID());
        }
    }

    /**
     * Returns estimated download time, in ms, for an application. Depends
     * directly from the parameters set on the attributes representing
     * parameters.
     *
     * @param application An instante of {@link Application}
     * @return estimated download time, in ms, for an application
     */
    public int downloadTime(Application application) {
        int downloadTime = 0;
        downloadTime = (int) ((application.getApplicationSize() * 8 / networkThroughput) * 1000);
//        System.out.println("Download Time " + downloadTime);
        return downloadTime;
    }

    // SECTION: PARAMETERS' SETTERS
    public void setApplicationLoads(Integer[] loads) {
        Collections.addAll(applicationLoads, loads);
    }

    public void setNodeCapacities(Integer[] capacities) {
        Collections.addAll(nodeCapacities, capacities);
    }

    public void setTaskGenerationIntervals(Integer[] taskGenerationInterval) {
        Collections.addAll(this.taskGenerationIntervals, taskGenerationInterval);
    }

    public void setThroughputs(Double[] throughputs) {
        Collections.addAll(this.throughputs, throughputs);
    }

    public void setRepetitions(int repetitions) {
        this.repetitions = repetitions;
    }

    public void setSimulationDuration(int simulationDuration) {
        this.simulationDuration = simulationDuration;
    }

    public void setAppQueue(RequestQueue appQueue) {
        this.appQueue = appQueue;
    }

    /**Sets application size mean in MB.
     * 
     * @param applicationSizeMean 
     */
    public void setApplicationSizeMean(double applicationSizeMean) {
        this.applicationSizeMean = applicationSizeMean;
    }

    /**Sets the deviation of application size in MB.
     * 
     * @param applicationSizeDeviation 
     */
    public void setApplicationSizeDeviation(double applicationSizeDeviation) {
        this.applicationSizeDeviation = applicationSizeDeviation;
    }

    /**Application ID. Represents the code.
     * 
     * @param applicationIdMean 
     */
    public void setApplicationIdMean(double applicationIdMean) {
        this.applicationIdMean = applicationIdMean;
    }

    
    /**Deviation of pplication ID. Represents the code.
     * 
     * @param applicationIdMean 
     */
    public void setApplicationIdDeviation(double applicationIdDeviation) {
        this.applicationIdDeviation = applicationIdDeviation;
    }

    /**Sets the ID of application instance. Represents the request.
     * 
     * @param instanceIdMean 
     */
    public void setInstanceIdMean(double instanceIdMean) {
        this.instanceIdMean = instanceIdMean;
    }

    /**Sets the deviation of the ID of application instance. Represents the request.
     * 
     * @param instanceIdMean 
     */
    public void setInstanceIdDeviation(double instanceIdDeviation) {
        this.instanceIdDeviation = instanceIdDeviation;
    }
    
    /**
     * A single simulation run. Creates FogNode, Queue, WaitingTimes, the
     * nextApplication. Determines the creation of new Applications in the
     * Queue. Adds Applications to the Queue. Checks wheter an Application
     * finished running and starts another execution. In the end, calculates
     * Statistics and prints results.
     *
     * @param duration The total duration of a simulation
     * @param taskGenerationInterval
     * @param fogNodeCapacity The capacity of a fog node in this simulation
     * @param taskLoad The reference value for calculate the loads of tasks
     * according to Statistical Distributions.
     * @return Ths simulation results.
     */
    public BasicSimulationResult simulation(int duration, int taskGenerationInterval, int fogNodeCapacity, int taskLoad) {
        // main simulation elements
        fogNode = new FogNode(fogNodeCapacity);
        ApplicationInstance applicationInstance;
        ApplicationInstance nextApp;
        double applicationInstanceId = 0.0;
        int lastTaskStartedAt = 0;
        //schedulers
        scheduleApplictions();
        int scheduledId;
        // metrics
        appQueue = fogNode.getAppQueue();
        waitingTimes = new ArrayList();
        totalLatencies = new ArrayList();
        countRequests = 0;
        countMetRequests = 0;
        double averageWait;
        BasicSimulationResult result;
        double queueSize;

        for (int currentSecond = 0; currentSecond < duration; currentSecond++) {
            lastTaskStartedAt = fogNode.tick();
            if (lastTaskStartedAt != 0) {
                // code to add response time (request/2). Or add all request times in the end;
                totalLatencies.add((double) currentSecond - lastTaskStartedAt);
                lastTaskStartedAt = 0;
            }
            if (this.newTask(taskGenerationInterval)) {   // new task in exponential average 180 milliseconds   
                //System.out.println("New task at " + currentSecond);
                countRequests = countRequests + 1;
                applicationInstanceId = Statistics.getPositiveNormalDistributionSamples(instanceIdMean, instanceIdDeviation);
                applicationInstance = new ApplicationInstance(currentSecond, taskLoad, (int) applicationInstanceId);
                if (fogNode.hasApplication(applicationInstance.getId())) {
                    countMetRequests = countMetRequests + 1;
                    // applicationInstance runs only when matches an Application within FogNode's Storage
                    appQueue.addLast(applicationInstance);
                }
//                appQueue.addLast(applicationInstance);

            }
            if (!fogNode.busy() & !appQueue.isEmpty()) {
                nextApp = (ApplicationInstance) appQueue.poll();
                // add request time (request/2) here
                waitingTimes.add((double) nextApp.waitTime(currentSecond)); // when fogNode is not busy, there's a difference from the timestamp it got and the current time
                fogNode.startNext(nextApp);
            }
            if (applicationScheduler.containsKey(currentSecond)) {
                fogNode.addApplication(applicationScheduler.get(currentSecond));
            }
        }
        // System.out.println("Waiting Times " + waitingTimes);
        // System.out.println("Total Latencies" + totalLatencies);
        // averageWait = Statistics.mean(waitingTimes); // results in waiting times
        averageWait = Statistics.mean(totalLatencies); // results in total latencies
        queueSize = appQueue.size();
        result = new BasicSimulationResult(queueSize); //adds queue size to the result
        result = new SimulationResult(result, averageWait); // adds averageWait/totalWait to the results
        result = new SimulationResult(result, countRequests);
        result = new SimulationResult(result, countMetRequests);
        result = new SimulationResult(result, ((double) countMetRequests / countRequests));

        System.out.println(result.getResult().toString());
        return result;
    }

    /**
     * Runs the simulations. Internally, loops the
     * {@link Simulation#simulation(int, int, int)} acording to the parameter
     * "repetitions". In the end, prints the simulation results. Requires
     * replacement with return statement.
     */
    public void run() {
        // auxiliary
        BasicSimulationResult aSimulation;
        int estimateRuns = throughputs.size() * nodeCapacities.size() * applicationLoads.size() * taskGenerationIntervals.size();
        System.out.println("ESTIMATE " + estimateRuns + " RUNS!");

        // results
        ArrayList<Double> averageWait;
        ArrayList<Double> countRequests;
        ArrayList<Double> countMetRequests;
        ArrayList<Double> percentRequests;
        double simulationAverage = 0;
        double standardDeviation = 0;
        String result = "";

        for (Double throughput : this.throughputs) {
            this.networkThroughput = throughput;
            for (Integer capacity : this.nodeCapacities) {
                for (Integer load : this.applicationLoads) {
                    for (Integer taskRate : this.taskGenerationIntervals) {
//                    System.out.println("# " + capacity + " # " + load + " # " + task );
                        simulationAverage = 0;
                        standardDeviation = 0;
                        averageWait = new ArrayList<>();
                        countRequests = new ArrayList<>();
                        countMetRequests = new ArrayList<>();
                        percentRequests = new ArrayList<>();

                        for (int i = 0; i < repetitions; i++) {
                            // this code means fog Node is 10 times powerful than the tasks it processes
                            System.out.println("*********************************************************************************************");
                            System.out.print("SIMULATION " + i);
                            System.out.print(" DURATION: " + simulationDuration);
                            System.out.print(" TASKS at " + taskRate);
                            System.out.print(" CAPACITY " + capacity);
                            System.out.print(" APPLICATION LOAD " + load);
                            System.out.print(" THROUGHPUT " + throughput);
                            System.out.println("");
                            // adds 
                            aSimulation = simulation(simulationDuration, taskRate, capacity, load);
                            averageWait.add(aSimulation.getResult().get(1));
                            countRequests.add(aSimulation.getResult().get(2));
                            countMetRequests.add(aSimulation.getResult().get(3));
                            percentRequests.add(aSimulation.getResult().get(4));

                        }
                        // Add new metrics here
                        simulationAverage = Statistics.mean(averageWait);
                        standardDeviation = Statistics.standardDeviation(averageWait);

                        // Sets result format
                        BasicExperimentResult basicResult = new BasicExperimentResult(capacity);
                        basicResult = new ExperimentResult(basicResult, load, false);
                        basicResult = new ExperimentResult(basicResult, taskRate, false);
                        basicResult = new ExperimentResult(basicResult, throughput);
                        // replicate those 4 lines do add more results
                        simulationAverage = Statistics.mean(averageWait);
                        standardDeviation = Statistics.standardDeviation(averageWait);
                        basicResult = new ExperimentResult(basicResult, simulationAverage);
                        basicResult = new ExperimentResult(basicResult, standardDeviation);

                        simulationAverage = Statistics.mean(countRequests);
                        standardDeviation = Statistics.standardDeviation(countRequests);
                        basicResult = new ExperimentResult(basicResult, simulationAverage);
                        basicResult = new ExperimentResult(basicResult, standardDeviation);

                        simulationAverage = Statistics.mean(countMetRequests);
                        standardDeviation = Statistics.standardDeviation(countMetRequests);
                        basicResult = new ExperimentResult(basicResult, simulationAverage);
                        basicResult = new ExperimentResult(basicResult, standardDeviation);

                        simulationAverage = Statistics.mean(percentRequests);
                        standardDeviation = Statistics.standardDeviation(percentRequests);
                        basicResult = new ExperimentResult(basicResult, simulationAverage);
                        basicResult = new ExperimentResult(basicResult, standardDeviation);

                        result = basicResult.getValue();
                        try {
                            resultFile.writeToFile(result);
                        } catch (IOException ex) {
                            System.out.println("Printing... \n " + result);
                            System.out.println(ex.getLocalizedMessage());
                        }
                        estimateRuns = estimateRuns - 1;
                        System.out.println("**************************************");
                        System.out.println("");
                        System.out.println(estimateRuns + " REMAINING... ");

                    }
                }
            }
        }

    }

    /**
     * The main method of the simulation.
     *
     * @param args
     */
//    public static void main(String[] args) {
//        Simulation s = new Simulation();  // application load
//        MainWindow mw;
//        s.setRepetitions(30);
//        s.setSimulationDuration(1 * 60 * 60 * 1000); // 3.600.000 ms or 1h
//        s.setTaskGenerationIntervals(new Integer[]{250});
//        s.setNodeCapacities(new Integer[]{5000});
//        s.setApplicationLoads(new Integer[]{5});
//        s.setThroughputs(new Double[]{1.0, 2.0});
//        s.run();
//        
//        mw = new MainWindow();
//        JOptionPane.showMessageDialog(mw, "Simulation has finished!", "FogWise", JOptionPane.INFORMATION_MESSAGE);
//    }
}
