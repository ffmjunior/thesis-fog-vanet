/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.Date;
import javax.swing.JOptionPane;
import simulation.Simulation;
import tuitei.Tuitei;
import twitter4j.TwitterException;
import util.Util;
import view.MainWindow;

/**
 *
 * @author ffmju
 */
public class Main {

    public static void main(String[] args) {

        String name = "20200330-001";
        String sim_name = "";

        Integer[] applicationIdMeans = Util.generateArray(90, 100, 2);
        Integer[] applicationIdDeviations = Util.generateArray(2, 10, 2 );
        Integer[] instanceIdMeans = Util.generateArray(90, 100, 2);
        Integer[] instanceIdDeviations = Util.generateArray(2, 10, 2);

        for (Integer applicationIdMean : applicationIdMeans) {
            for (Integer applicationIdDeviation : applicationIdDeviations) {
                for (Integer instanceIdMean : instanceIdMeans) {
                    for (Integer instanceIdDeviation : instanceIdDeviations) {
                        sim_name = name + "-" + applicationIdMean + "-" + applicationIdDeviation
                                + "-" + instanceIdMean + "-" + instanceIdDeviation + "-";
                        System.out.println(sim_name);
                        experiment(sim_name, applicationIdMean, applicationIdDeviation,
                                instanceIdMean, instanceIdDeviation);

                    }
                }
            }
        }
        ending(name);

    }

    public static void experiment(String name, int applicationIdMean,
            int applicationIdDeviation, int instanceIdMean,
            int instanceIdDeviation) {

        Simulation s = new Simulation(name);  // application load

        s.setRepetitions(10);
        s.setSimulationDuration(1 * 60 * 60 * 1000); // 3.600.000 ms or 1h

        Integer[] taskGenerationIntervals = Util.generateArray(100, 1000, 300);
        //Integer[] taskGenerationIntervals = new Integer[]{250};
        s.setTaskGenerationIntervals(taskGenerationIntervals);

        // Integer[] nodeCapacities = Util.generateArray(1000, 2000, 100);
        Integer[] nodeCapacities = new Integer[]{10000};
        s.setNodeCapacities(nodeCapacities);

        // Integer[] applicationLoads = Util.generateArray(1000, 20000, 5000);
        Integer[] applicationLoads = new Integer[]{1000};
        s.setApplicationLoads(applicationLoads);

        // Double[] throughputs = Util.generateArray(0.0, 10, 0.5);
        Double[] throughputs = new Double[]{20.0};
        s.setThroughputs(throughputs);

        // Application Size
        s.setApplicationSizeMean(2);
        s.setApplicationSizeDeviation(1);

        // Application ID
        s.setApplicationIdMean(applicationIdMean);
        s.setApplicationIdDeviation(applicationIdDeviation);

        // Request ID
        s.setInstanceIdMean(instanceIdMean);
        s.setInstanceIdDeviation(instanceIdDeviation);

        s.run();
    }

    public static void ending(String name) {
        MainWindow mw;
        Date date = new Date();
        long time = date.getTime();
        System.out.println("Time " + time);

        date = new Date();
        time = date.getTime() - time;
        String msg = "Simulation " + name + " finished after " + Util.millisecondsToDuration(time) + "! Everything seems ok!";

        Tuitei tuitei = new Tuitei();
        try {
            tuitei.tuitar(msg);
        } catch (TwitterException ex) {
            System.out.println("Couldn't tweet!");
        }

        mw = new MainWindow();
        JOptionPane.showMessageDialog(mw, msg, "FogWise", JOptionPane.INFORMATION_MESSAGE);
    }

}
