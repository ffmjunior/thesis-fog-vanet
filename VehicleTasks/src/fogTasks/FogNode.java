/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fogTasks;

import util.ApplicationStorage;
import util.RequestQueue;

/**
 *
 * @author junior
 */
public class FogNode {

    private int capacity;
    private ApplicationInstance currentTask;
    private int timeRemaining;

    // working
    private RequestQueue requestQueue;
    private ApplicationStorage applicationStorage;
    
    /**
     * Represent any Cloud or Fog Node in a simulation. The {@link FogNode}
     * compares its capacity to the {@link ApplicationInstance#load} to
     * determine the execution time of this applications. The more the capacity,
     * the less the execution time.
     *
     * @param processingCapacity The relative Processing Capacity of a Fog Node.
     * @param currentApplication The Application that is currently running
     * @param timeRemaining The time remaing to the Application completion.
     */
    public FogNode(int processingCapacity, ApplicationInstance currentApplication, int timeRemaining) {
        this.capacity = processingCapacity;
        this.currentTask = currentApplication;
        this.timeRemaining = timeRemaining;
        requestQueue = new RequestQueue();
        applicationStorage = new ApplicationStorage(1000);
    }

    public FogNode(int taskRate) {
        this(taskRate, null, 0);
    }

    /**
     * Decrements one to the simulation time. In the current resolution,
     * decrements one millisecond for the current Application execution time.
     * Returns zero wheter or not there's an application running, or a timestamp
     * if an application has finished running.
     *
     * @return The initial timestamp of the finished task. Zero otherwise.
     */
    public int tick() {
        int taskStartedAt = 0;
        if (this.currentTask != null) {
            currentTask.tick();
            if (currentTask.isFinished()) {
                taskStartedAt = this.currentTask.getInitialTimestamp();
                this.currentTask = null;
            }
        }
        return taskStartedAt;
    }

    /**
     *
     * @return True, if the FogNode is running any applications at that tick.
     */
    public boolean busy() {
        if (this.currentTask != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Assigns a new task from waiting Queue to execution. This method helps
     * determining the execution time of this task by dividing load of the
     * {@link ApplicationInstance} and the capacity of the {@link FogNode}. The
     * {@link FogNode} compares its capacity to the
     * {@link ApplicationInstance#load} to determine the execution time of this
     * applications. The more the capacity, the less the execution time.
     *
     * @param newTask
     */
    public void startNext(ApplicationInstance newTask) {
        this.currentTask = newTask;
        currentTask.setDuration(newTask.getLoad() * 1000 / this.capacity);
    }

    public ApplicationInstance getCurrentTask() {
        return currentTask;
    }

    /**
     * Returns a reference to the internal queue, to be used in simulation.
     * @return 
     */
    public RequestQueue getAppQueue() {
        return requestQueue;
    }
    
    // methods for handling storage
    
    public void addApplication(Application a){
        applicationStorage.add(a);
    }
    
    public boolean hasApplication(int applicationId){
        return applicationStorage.hasApplication(applicationId);
    }
    
    

}
