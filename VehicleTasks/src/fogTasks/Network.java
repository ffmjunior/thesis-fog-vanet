/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fogTasks;

/**
 *
 * @author junior kbca
 */
public interface Network {
    
    public abstract double getBandwidth();
    public abstract double getRTT();
    public abstract double getDelay();
}
