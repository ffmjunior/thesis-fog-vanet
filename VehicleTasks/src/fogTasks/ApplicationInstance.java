/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fogTasks;

import org.apache.commons.math3.distribution.NormalDistribution;
import util.IdComparison;

/**
 * Represents an application instance. Is one of the core elements of this 
 * simulation. Holds several parameters used in a simulation.
 * @author junior
 */
public class ApplicationInstance implements IdComparison {

    protected int id;
    protected double load;
    protected int initialTimestamp;
    protected double duration;
    protected int applicationType;
    protected int codeSize; // future use

    public ApplicationInstance(int time) {
        this(time, 20);
    }

    /**
     * An Application Instance. Works as a task that goes to a queue in the
     * {@link FogNode}. The time receives the timestamp on the start of the
     * Application. The {@link FogNode} compares its capacity to the
     * {@link ApplicationInstance#load} to determine the execution time of this
     * applications. The more the capacity, the less the execution time.
     *
     * @param time Receives the timestamp when the task starts.
     * @param load The value compared to the capacity of the {@link FogNode}
     */
    public ApplicationInstance(int time, int load) {
        this.initialTimestamp = time;
        this.load = this.calculateLoad(load, load * 0.5); // Changes load
    }

    public ApplicationInstance(int time, int load, int applicationInstanceId) {
        this(time, load);
        this.id = applicationInstanceId;
    }

    /**
     * Returns the waiting time of an application. The {@link FogNode} uses this
     * method in their {@link Simulation#simulation(int, int, int)} method to
     * calculate the time remaing for the end of the task or the times that the
     * task is waiting in the queue.
     *
     * @param currentTime The current time of the {@link Simulation}
     *
     * @return The time the task is waiting for service.
     */
    public int waitTime(int currentTime) {
        return currentTime - this.initialTimestamp;
    }

    public void tick() {
        this.duration = duration - 1;
    }
    
    
    @Override
    public int getId() {
        return id;
    }
    
    

    public void setDuration(double duration) {
        this.duration = duration;
//        System.out.println("Duration " + this.duration + " Load " + this.load);
    }

    public boolean isFinished() {
        if (this.duration <= 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Calculates loads to the {@link AbstractApplicationInstance}. Internally
     * uses the reference value and deviation to feed a UniformDistribution. The
     * greater the deviation value, the smaller the difference. Sets upperbound
     * to reference+reference/deviation and lowerbound to
     * reference+reference/deviation.
     *
     * @param reference
     * @param deviation
     * @return
     */
    public double calculateLoad(double reference, double deviation) {
        NormalDistribution nd = new NormalDistribution(reference, deviation);
        load = nd.sample();
        while(load < 0){
           load = nd.sample(); 
        }        
        return load;
    }

    /**
     *
     * @return The timestamp of the creation of this Application Instance
     */
    public int getInitialTimestamp() {
        return initialTimestamp;
    }

    /**
     *
     * @return The load of this Application Instance
     */
    public double getLoad() {
        return load;
    }

    @Override
    public boolean compareIDs(IdComparison ic) {
        return this.getId() == ic.getId();
    }


}
