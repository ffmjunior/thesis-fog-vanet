/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fogTasks;

/**
 * Should have information to simulate cars approximating and moving away 
 * also based on tick. My tick is set to ms resolution, so I need to calcula
 * @author junior kbca
 */
public class Request {
    
    int id;
    double dataSize;
    double rtt;
    double distance;
    double speed;
    boolean stationary;
    boolean approximating;
    
}
