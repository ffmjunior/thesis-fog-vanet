/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fogTasks;

/**
 * An application representing an application type.
 * @author junior kbca
 */
public class Application {

    /**
     * One can initilize applications by application size only.
     * @param applicationSize the size of all application instantes derived from this application.
     */
    public Application(double applicationSize) {
        this.applicationSize = applicationSize;
    }

    /**
     * One can initilize applications by application size and application id.
     * @param applicationSize the size of all application instantes derived from this application.
     * @param applicationID the id of all application instances derived from this application.
     */
    public Application(double applicationSize, int applicationID) {
        this.applicationSize = applicationSize;
        this.applicationID = applicationID;
    }
    
    private double applicationSize;
    private int applicationID;

    /**
     * 
     * @return The application size.
     */
    public double getApplicationSize() {
        return applicationSize;
    }

    public void setApplicationSize(double applicationSize) {
        this.applicationSize = applicationSize;
    }

    public int getApplicationID() {
        return applicationID;
    }
    
    
    
    
    
}
