/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package results.simulation;

import java.util.ArrayList;

/**
 * Contains the results from a single simulation. 
 * @author junior
 */
public class BasicSimulationResult {
    
    private ArrayList<Double> result;
    
    public BasicSimulationResult(double value){
        result = new ArrayList<>();
        result.add(value);
    }
    
    public ArrayList<Double> getResult() {
        return result;
    }
    
}

