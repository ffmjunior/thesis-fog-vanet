/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package results.simulation;

import java.util.ArrayList;

/**
 *
 * @author junior kbca
 */
public abstract class SimulationResultDecorator extends BasicSimulationResult {

    BasicSimulationResult simulationResult;

    public SimulationResultDecorator(BasicSimulationResult simulationResult, double value) {
        super(value);
        this.simulationResult = simulationResult;
    }

    public ArrayList<Double> getResult() {
        ArrayList<Double> allResults = this.simulationResult.getResult();
        allResults.addAll(super.getResult());
//        super.getResult().addAll(this.simulationResult.getResult());
        // return super.getResult();
        // return this.simulationResult.getResult();
        return allResults;
    }
}
