/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package results.experiment;

/**
 * Composes complex results from simulations. Each BasicResult represents an 
 * column in the results that generate CSV.
 * @author junior kbca
 */
public class BasicExperimentResult {

    Double value;
    Boolean aspas;

    /**
     * Instatiates a BasicResult. 
     * @param value a value from the results
     * @param aspas true if its a literal, that is, a factor to display in the 
     * graphs
     */
    public BasicExperimentResult(Double value, Boolean aspas) {
        this.value = value;
        this.aspas = aspas;
    }

    /**
     * Instatiates a BasicResult. Sets the aspas to false.
     * @param value a value from the results
     */
    public BasicExperimentResult(Double value) {
        this(value, false);
    }
    
    /**
     * Instatiates a BasicResult. Internally, casts Integer to Double.
     * @param value a value from the results
     */
    public BasicExperimentResult(Integer value, Boolean aspas) {
        this.value = value.doubleValue();
        this.aspas = aspas;
    }

    /**
     * Instatiates a BasicResult. Sets the aspas to false. Internally, casts 
     * Integer to Double.
     * @param value a value from the results
     */
    public BasicExperimentResult(Integer value) {
        this(value.doubleValue(), false);
    }

    /**
     * Returns the value according to the parameter {@link BasicExperimentResult#aspas}
     * @return 
     */
    public String getValue() {
        String theValue = value + "";
        if (aspas) {
            theValue = "'" + theValue + "'";
        }
        return theValue;
    }

}
