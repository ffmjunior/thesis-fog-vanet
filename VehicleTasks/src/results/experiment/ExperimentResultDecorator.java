/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package results.experiment;

/**
 * Allows easy extension to the number of columns generated from a experiment.
 * Simply add values to the Decorator to create the desired number of columns.
 * @author junior kbca
 */
public abstract class ExperimentResultDecorator extends BasicExperimentResult {
    
    BasicExperimentResult experimentResult;

    public ExperimentResultDecorator(BasicExperimentResult experimentResult, Double value, Boolean aspas) {
        super(value, aspas);
        this.experimentResult = experimentResult;
    }

    @Override
    public String getValue() {
        return this.experimentResult.getValue() + "," +super.getValue(); //To change body of generated methods, choose Tools | Templates.
    }
    
}
