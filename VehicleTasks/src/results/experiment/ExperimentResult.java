/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package results.experiment;

/**
 * Constains the result of the complete simulation. After all repetitions.
 * @author junior kbca
 */
public class ExperimentResult extends ExperimentResultDecorator{    

    public ExperimentResult(BasicExperimentResult experimentResult, Double value, Boolean aspas) {
        super(experimentResult, value, aspas);
    }
    
    public ExperimentResult(BasicExperimentResult experimentResult, Integer value, Boolean aspas) {
        super(experimentResult, value.doubleValue(), aspas);
    }
    
    public ExperimentResult(BasicExperimentResult experimentResult, Double value) {
        super(experimentResult, value, false);
    }

    public ExperimentResult(BasicExperimentResult experimentResult, Integer value) {
        super(experimentResult, value.doubleValue(), false);
    }

    
    
}
