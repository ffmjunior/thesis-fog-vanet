    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 Based on: https://stackoverflow.com/questions/20039980/java-replace-line-in-text-file
 */
package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Based on https://www.homeandlearn.co.uk/java/write_to_textfile.html
 *
 * @author junior kbca
 */
public class FileTools {

    private String path;
    private boolean append;
    List<String> fileContent;

    public FileTools() {
        this("/home/junior/SSD/Dev/thesis-fog-vanet/RScripts/plot.R");
    }

    public FileTools(String path) {
        this.path = path;
        this.append = false;
        fileContent = new ArrayList<>();
        this.setAppend(false);
        System.out.println("PATH " + this.path);
    }

    private void setAppend(boolean append) {
        this.append = append;
    }

    public void readFromFile(String fullPath) throws FileNotFoundException, IOException {
        FileReader fileReader;
        BufferedReader bufferedReader;
        String line = "";

        fileReader = new FileReader(fullPath);
        bufferedReader = new BufferedReader(fileReader);

        while ((line = bufferedReader.readLine()) != null) {
            fileContent.add(line);
        }

        for (String aLine : fileContent) {
            System.out.println("LINE: " + aLine);
        }

        bufferedReader.close();
    }

    public void writeToFile(String line) throws IOException {
        System.out.println("WRITING " + line + " \n  TO " + this.path);
        File file = new File(path);
        FileWriter write = new FileWriter(path, append);
        PrintWriter printLine = new PrintWriter(write);
//        printLine.printf("%s" + "%n", line);
        printLine.printf(line + "\n");
        printLine.close();

    }

    public void replace(String contains, String newLine) {
        FileReader fileReader;
        BufferedReader bufferedReader;
        String line = "";

        try {
            fileReader = new FileReader(this.path);
            bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {
                fileContent.add(line);
            }

            for (String aLine : fileContent) {
                System.out.println("LINE: " + aLine);
            }

            bufferedReader.close();

            for (String aLine : fileContent) {
                this.writeToFile(aLine);
            }

        } catch (FileNotFoundException ex) {
            System.err.println("File not found. Rolling back to defaults!");
        } catch (IOException ex) {
            System.err.println("Can't process file.  Rolling back to defaults!");
        }

    }

}
