/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import fogTasks.Application;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author junior kbca
 */
public class ApplicationStorage {

    public ApplicationStorage(double storage, double free) {
        this.storage = storage;
        this.free = free;
    }

    public ApplicationStorage(double storage) {
        this(storage, storage);
        storageMap = new HashMap<>();
    }
    
    Map<Integer, Application> storageMap;
    double storage;
    double free;

    public boolean add(Application e) {
        if (e.getApplicationSize() < this.free) {
            free = free - e.getApplicationSize(); // posso pensar em opções com ou sem repetição de código.
            storageMap.put(e.getApplicationID(), e);
            return true;
        } else {
            return false;
        }
    }

    public void remove(Application e) {
        free = free + e.getApplicationSize();
        storageMap.remove(e.getApplicationID()); 
    }

    public boolean hasApplication(int id) {
        return storageMap.containsKey(id);
    }
    
    public boolean hasApplication(Application e) {
        return storageMap.containsKey(e.getApplicationID());
    }

}
