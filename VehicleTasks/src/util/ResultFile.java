    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Based on https://www.homeandlearn.co.uk/java/write_to_textfile.html
 *
 * @author junior kbca
 */
public class ResultFile {

    private String path;
    private boolean append;

    public ResultFile() {
        this("D:/Dev/thesis-fog-vanet/Scripts/results.csv", "capacity, load, taskRate, mean, sd", false);
    }

    public ResultFile(String path, String header) {
        this("D:/Dev/thesis-fog-vanet/Scripts/results.csv", header, false);
    }

    public ResultFile(String path, String header, boolean append) {
        this.path = path;
        this.append = append;
        try {
            this.writeToFile(header);
        } catch (IOException ex) {
            System.out.println("ERRO " + ex.getLocalizedMessage());

        }
        this.setAppend(true);
    }

    private void setAppend(boolean append) {
        this.append = append;
    }

    public boolean writeToFile(String line) throws IOException {
        File file = new File(path);
        FileWriter write = new FileWriter(path, append);
        PrintWriter printLine = new PrintWriter(write);
//        printLine.printf("%s" + "%n", line);
        printLine.printf(line + "\n");
        printLine.close();
        return false;
    }

}
