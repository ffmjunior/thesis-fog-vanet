/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author junior kbca
 */
public interface IdComparison {
    
    public boolean compareIDs(IdComparison ic);
    public int getId();
    
}
