/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.ArrayList;
import java.util.Collections;
import org.apache.commons.math3.distribution.NormalDistribution;

/**
 *
 * @author junior kbca
 */
public class Util {

    public static Integer[] generateArray(int start, int end, int step) {
        System.out.println("####################################");
        int size = ((end - start) / step) + 1;
        System.out.println("Size " + size);
        Integer[] nums = new Integer[size];
        for (int i = 0; i < size; i++) {
            nums[i] = start + step * i;
            System.out.println(nums[i]);
        }
        return nums;
    }
    
    public static Double[] generateArray(double start, double end, double step) {
        System.out.println("####################################");
        int size = (int) (((end - start) / step) + 1);
        System.out.println("Size " + size);
        Double[] nums = new Double[size];
        for (int i = 0; i < size; i++) {
            nums[i] = start + step * i;
            System.out.println(nums[i]);
        }
        return nums;
    }

    public static ArrayList<Object> convert(Object[] someArray) {
        ArrayList<Object> objects = new ArrayList<Object>();
        Collections.addAll(objects, someArray);
        return objects;
    }

    public static String millisecondsToDuration(long timeMilliseconds) {
        String duration = "";
        long hours = (timeMilliseconds / 1000) / 60 / 60;
        long minutes = (timeMilliseconds / 1000) / 60 % 60;
        long seconds = (timeMilliseconds / 1000) % 60;
        duration = timeMilliseconds + " Milliseconds = " + hours + " hour(s) and " + minutes + " minutes and " + seconds + " seconds.";
        return duration;
    }

    public static double calculateLoad(double reference, double deviation) {
        NormalDistribution nd = new NormalDistribution(reference, deviation);
        Double d = nd.sample();
        return d;
    }

    public static void main(String[] args) {

        double reference = 10;
        for (int i = 0; i < 10; i++) {
            System.out.println(calculateLoad(reference, reference * 0.1));

        }
    }
}
