/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.ArrayList;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;

/**
 *
 * @author junior kbca
 */
public class Statistics {

    /**
     * Calculates the mean of the values in an {@link ArrayList} of valid
     * Doubles. Uses {@link Statistics#mean(java.util.ArrayList) } and 
     * {@link Statistics#sum(java.util.ArrayList)
     * }
     *
     * @param doubles An {@link ArrayList} of valid Doubles
     * @return The average of the input values
     */
    public static double mean(ArrayList<Double> doubles) {
        return sum(doubles) / doubles.size();
    }

    /**
     * Calculates the Sum of the values of an {@link ArrayList} of valid
     * Doubles.
     *
     * @param doubles An {@link ArrayList} of valid Doubles
     * @return The sum of the input values
     */
    public static double sum(ArrayList<Double> doubles) {
        double sum = 0;
        for (Double aDouble : doubles) {
            sum = sum + aDouble;
        }
        return sum;
    }

    /**
     * Calculates the Standard Deviation of an {@link ArrayList} of valid
     * Doubles. Uses {@link Statistics#mean(java.util.ArrayList) } and 
     * {@link Statistics#sum(java.util.ArrayList)
     * }
     *
     * @param doubles An {@link ArrayList} of valid Doubles
     * @return The Standard Deviation of the input values
     */
    public static double standardDeviation(ArrayList<Double> doubles) {
        double sd = 0;
        double mainAverage = mean(doubles);
        double sdAverage;
        ArrayList<Double> differences = new ArrayList<>();
        for (Double aDouble : doubles) {
            differences.add(Math.pow(mainAverage - aDouble, 2));
        }
        sdAverage = mean(differences);
        sd = Math.sqrt(sdAverage);
        return sd;
    }

    public static int [] getUniformDistributionSamples(int lowerBound, int upperBound, int sampleSize) {
        UniformIntegerDistribution uid = new UniformIntegerDistribution(lowerBound, upperBound);
        return uid.sample(sampleSize);
    }
    
    public static int getUniformDistributionSample(int lowerBound, int upperBound) {
        UniformIntegerDistribution uid = new UniformIntegerDistribution(lowerBound, upperBound);
        return uid.sample();
    }
    
    public static double [] getNormalDistributionSamples(double mean, double deviation, int sampleSize) {
        NormalDistribution nd = new NormalDistribution(mean, deviation);
        return nd.sample(sampleSize);
    }
    
    public static double getNormalDistributionSamples(double mean, double deviation) {
        NormalDistribution nd = new NormalDistribution(mean, deviation);
        return nd.sample();
    }
    
    public static double getPositiveNormalDistributionSamples(double mean, double deviation) {
        double sample = getNormalDistributionSamples(mean, deviation);
        if (sample <= 0 ){
            sample = getPositiveNormalDistributionSamples(mean, deviation);
        }
        return sample;
    }

    /**
     * Generates a random integer number within interval. Internally, uses 
     * UniformDistribution.
     * @param lowerBound
     * @param upperBound
     * @return 
     */
    public int randomIdGenerator(int lowerBound, int upperBound) {
        UniformIntegerDistribution uid
                = new UniformIntegerDistribution(lowerBound, upperBound);
        return uid.sample();
    }

    /**
     * Generates a random integer number within interval. Internally, uses 
     * Normal distribution based on a mean.
     * @param mean
     * @param deviation
     * @return 
     */
    public int randomIdGenerator(double mean, double deviation) {
        NormalDistribution nd = new NormalDistribution(mean, deviation);
        return (int) nd.sample();
    }

}
