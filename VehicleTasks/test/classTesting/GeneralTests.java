package classTesting;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.apache.commons.math3.distribution.AbstractRealDistribution;
import org.apache.commons.math3.distribution.GeometricDistribution;
import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.distribution.NormalDistribution;
import org.apache.commons.math3.distribution.RealDistribution;
import org.apache.commons.math3.distribution.IntegerDistribution;
import org.apache.commons.math3.distribution.UniformIntegerDistribution;
import org.apache.commons.math3.distribution.ExponentialDistribution;
import org.junit.Test;
import results.experiment.ExperimentResult;
import results.experiment.BasicExperimentResult;
import static org.junit.Assert.*;
import util.ResultFile;

/**
 *
 * @author junior kbca
 */
public class GeneralTests {
    
    public static void main(String[] args) {
        GeneralTests gt = new GeneralTests();
        TDistribution td = new TDistribution(29);
//        gt.dist(td);

        NormalDistribution nd = new NormalDistribution(5, 1);
        gt.dist(nd);

        GeometricDistribution geometricDistribution = new GeometricDistribution(0.1);
//        gt.dist(geometricDistribution);

        UniformIntegerDistribution uniformIntegerDistribution
                = new UniformIntegerDistribution(1, 16);
//        gt.dist(uniformIntegerDistribution);

        ExponentialDistribution ed = new ExponentialDistribution(180);
//        gt.dist(ed);
        
    }
    
    public void dist(AbstractRealDistribution distribution) {
        RealDistribution d = distribution;
        System.out.println("\n" + d.getClass().getSimpleName());
        ResultFile file = new ResultFile("D:/Dev/thesis-fog-vanet/Scripts/tests.csv", "samples");
        double lowerTail = d.cumulativeProbability(0);     // P(T(29) <= -2.656)
        double upperTail = 1.0 - d.cumulativeProbability(distribution.getNumericalMean() * 2); // P(T(29) >= 2.75)
        System.out.println("Lower Tail "
                + lowerTail
                + " UpperTail "
                + upperTail);
        double[] samples = d.sample(100);
        
        System.out.println("\nAverage " + this.mean(samples));
    }
    
   
    
    public void dist(IntegerDistribution distribution) {
        IntegerDistribution d = distribution;
        System.out.println("\n" + d.getClass().getSimpleName());
        ResultFile file = new ResultFile("D:/Dev/thesis-fog-vanet/Scripts/tests.csv", "samples");
        double lowerTail = d.cumulativeProbability(0);     // P(T(29) <= -2.656)
        double upperTail = 1.0 - d.cumulativeProbability(10); // P(T(29) >= 2.75)
        System.out.println("Lower Tail " + lowerTail + " UpperTail " + upperTail);
        int[] samples = d.sample(100);
        
        for (double sample : samples) {
            System.out.print(sample + " ");
        }
    }
    
    public double sum(double [] numbers){
        double sum = 0;
        for (double number : numbers) {
            System.out.print((int) number + " ");
            sum = sum + number;
        }
        return sum;
    }
    
    public double mean(double [] numbers){
        double sum = sum(numbers);
        return sum / numbers.length;
    }
    
    @Test
    public void testExperimentResultDecorator() {
        Integer i = 6;
        BasicExperimentResult r = new ExperimentResult(new BasicExperimentResult(i.doubleValue(), true), 6.0, true);
        BasicExperimentResult s = new ExperimentResult(new ExperimentResult(new BasicExperimentResult(5.0), 6.0), 7.0);
        System.out.println(s.getValue());
        assertEquals(r.getValue(), "'6.0','6.0'");
    }
    
}
