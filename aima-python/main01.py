from agents import *

def random_agent():
    list = ['Right', 'Left', 'Suck', 'NoOp']
    program = RandomAgentProgram(list)
    agent = Agent(program)
    environment = TrivialVacuumEnvironment()
    environment.add_thing(agent)
    environment.run()
    result = environment.status == {(1, 0): 'Clean' , (0, 0): 'Clean'}
    return result

def random_vacuum_agent():
    agent = RandomVacuumAgent()
    environment = TrivialVacuumEnvironment()
    environment.add_thing(agent)
    environment.run()
    result = environment.status == {(1, 0): 'Clean', (0, 0): 'Clean'}
    return result

def reflex_vacuum_agent():
    agent = ReflexVacuumAgent()
    environment = TrivialVacuumEnvironment()
    environment.add_thing(agent)
    environment.run()
    result = environment.status == {(1, 0): 'Clean', (0, 0): 'Clean'}
    return result

def model_based_agent():
    agent = ModelBasedVacuumAgent()
    environment = TrivialVacuumEnvironment()
    environment.add_thing(agent)
    environment.run()
    result = environment.status == {(1, 0): 'Clean', (0, 0): 'Clean'}
    print("Desempenho {}".format(agent.performance))
    return result

if __name__ == '__main__':
    for i in range(30):
        model_based_agent()