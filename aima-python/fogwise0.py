from agents import *
import statistics

loc_A, loc_B = (0, 0), (1, 0)   # Dois locais com características diferentes para requisições
env_types = [50, 20, 10, 5, 0]              # Requisição presente 1, ou não presente 0. 1 pode ser trocado por qualquer valor
env_weights = [1, 1, 1, 1, 1]

class VehicleAgent(Agent):

    def __init__(self, program=None, capacity=None):
        super().__init__(program)
        self.capacity = capacity
        if program is None or not isinstance(program, collections.Callable):
            model = {loc_A: None, loc_B: None}  # modelo simples com dois locais onde aparecerão (ou não) requisições

            def program(percept):
                location, status = percept
                model[location] = status  # aqui está atualizando o modelo
                if model[loc_A] == model[loc_B] == 0:
                    return 'NoRep'
                elif status > 0:  # é aqui que eu começo a dar a inteligência
                # (capacity, status)
                    if self.capacity > status * 1000:
                        return 'Rep'
                    else:
                        return 'NoRep'
                elif location == loc_A:
                    return 'R'
                elif location == loc_B:
                    return 'L'
        self.program = program

def SimpleModelBasedVehicle(capacity):
    """baseado fortemente em ModelBasedVacuumAgent.
    Agora está sendo substituído pela implementação interna, de forma que seja
    possível utilizar os atributos do agente para decidir sobre a resposta à
    requisição. Ao mesmo tempo, eu tenho que investigar se isso não pode gerar
    um problema de escalabilidade mais na frente, ou seja, pode ser que fazendo
    internamente, eu não consiga estender para outras interações entre agentes
    e ambientes.

    """

    model = {loc_A: None, loc_B: None} # modelo simples com dois locais onde aparecerão (ou não) requisições

    def program(percept):
        location, status = percept
        model[location] = status # aqui está atualizando o modelo
        if model[loc_A] == model[loc_B] == 0:
            return 'NoRep'
        elif status > 0: # é aqui que eu começo a dar a inteligência
            #(capacity, status)
            if capacity > status * 100:
                return 'Rep'
            else:
                return 'NoRep'
        elif location == loc_A:
            return 'R'
        elif location == loc_B:
            return 'L'
    return VehicleAgent(program)

class FogWiseEnv0(Environment): #baseado fortemente em TrivialVacuumEnvironment
    """Baseado fortemente em TrivialVacuumEnvironment
    Deve conter os apenas as características necessárias para a tomada de
    decisão sobre responder, ou não, a uma requisição. Entre elas, destacam-se
    a distância em relação à posição contida na mensagem, alguma previsão sobre
    afastamento ou não, a tabela SDNCar, e outras informações do veículo ou da
    aplicação como, resultado de um profiling aproximado, capacidade conhecida
    do veículo, tempo das aplicações, entre outros detalhes. Inicialmente
    (12/03), as características são relação requisição/aplicações (figura 5.9)

    """

    def __init__(self):
        super().__init__()
        self.reset_env_status()

    def reset_env_status(self):
        self.status = {loc_A: random.choices(env_types, weights=env_weights)[0],
                       loc_B: random.choices(env_types, weights=env_weights)[0] }
        # print("Status {}".format(self.status))


    def thing_classes(self):
        """tenho que traduzir esses elementos para elementos que se encaixem no
        ambiente que estou me propondo a avaliar. Ex:
        Dirt == requisiçao
        ModelBasedVacuumAgent == Veículo
        Wall == por enquanto, nada, mas pode ser uma abstração de aplicação

        :return:
        """
        return [Wall, Dirt, ReflexVacuumAgent, RandomVacuumAgent,
                TableDrivenVacuumAgent, ModelBasedVacuumAgent]

    def percept(self, agent):
        return (agent.location, self.status[agent.location])

    def execute_action(self, agent, action):
        if action == 'R':
            agent.location == loc_B
            agent.performance = agent.performance - 1
        elif action == 'L':
            agent.location == loc_A
            agent.performance = agent.performance - 1
        elif action == "Rep":
            if self.status[agent.location] > 0:
                agent.performance = agent.performance + 10
            self.status[agent.location] = 0

    def default_location(self, thing):
        return random.choice([loc_A, loc_B])

    def step(self):
        super().step()
        self.reset_env_status()


def run_FogWiseEnv0(capacity=0):
    # agent = SimpleModelBasedVehicle(capacity)
    agent = VehicleAgent(capacity=capacity)
    environment = FogWiseEnv0()
    environment.add_thing(agent)
    environment.run()
    result = agent.performance
    # print("Desempenho {}".format(result))
    return result

if __name__ == '__main__':
    results = []
    for i in range(100):
        results.append(run_FogWiseEnv0(capacity=15000))

    mean = statistics.mean(results)
    stdev = statistics.stdev(results)

    print("Mean {} StDev {} VALUES = {} ".format(mean, stdev, results))

# ______________________________________________________________________________________________________________________
