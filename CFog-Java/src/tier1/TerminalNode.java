/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tier1;

import abstractions.Application;
import abstractions.Hardware;
import abstractions.Location;
import abstractions.Events;
import java.util.ArrayList;

/**
 *
 * @author junior
 */
public class TerminalNode {
    Integer id;
    Boolean status;
    Events [] eventTypes;
    Location location;
    Hardware hardware;
    ArrayList<Application> runningApplications; // Ici. May use a Factory here.
    ArrayList<Application> storedApplications; // Ici. May use a factory here.
    
}
