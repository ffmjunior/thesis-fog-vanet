/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package distributions;

import org.apache.commons.math3.distribution.TDistribution;
import org.apache.commons.math3.random.RandomDataGenerator;

/**
 *
 * @author junior kbca
 */
public class Distributions {

    public void distribution() {

        TDistribution distribution = new TDistribution(1);
            
        
        double lowerTail = distribution.cumulativeProbability(-2.656);
        double upperTail = 1.0 - distribution.cumulativeProbability(1);

        System.out.println(lowerTail + " / " + upperTail);
        for (int i = 0; i < 10; i++) {
            System.out.print(distribution.sample() + " / ");
        }

    }

    public static void main(String[] args) {
        Distributions d = new Distributions();
        d.distribution();
    }

}
