#!/usr/bin/python
# -*- coding: utf-8 -*-

import math
import matplotlib.pyplot as plot
import teste

def contact_duration():
    """

    :return:
    """

def request_rate(speed=14, duration=36000, range=200, street_width=9,
                 percent=1, app_rate=10):
    """The IoT applications counter.
    This script estimates the total requisitions a vehicle may need to respond
    depending on the environment. This script estimates 1 IoT device per
    squaremeter.

    Is the rate just probabilistic? That is, how many requisitions a car can
    receive depending on the situation? Is there situations when it will
    receive more or less requisitions?

    Inputs: speed (m/s), duration(s), range(m), street width(m), applications
    percent(range(1:100)), application data rate (Hz)

    Output: requisitions per second

    Mostly based on "RENDA, M. Elena et al. IEEE 802.11 p VANets: Experimental
    evaluation of packet inter-reception time. Computer Communications,
    v. 75, p. 26-38, 2016."
    speed = 14 m/s
    range = 170~200m
    application data rate = 10 Hz (50ms implies 20 Hz maximum)
    """
    range_area = math.pow(range, 2) * math.pi
    street_area = street_width * range
    applications_second = speed * street_area * percent
    requests_second = applications_second * app_rate
    # print(range_area, street_area, applications_second, requests_second)
    return requests_second

if __name__ == '__main__':
    speeds = []
    requests = []
    ranges = []
    for i in range(10, 200):
        for j in range(1, 20):
            speeds.append(i)
            ranges.append(j)
            requests.append(request_rate(range=i, speed=j))
    # plot.plot(speeds, requests, "b")

    teste.plot_3db(speeds, requests, ranges)

"""
    speeds = []
    requests = []
    ranges = []
    for i in range(10, 200):
        for j in range(1, 20):
            speeds.append(i)
            ranges.append(j)
            requests.append(request_rate(range=i, speed=j))
    # plot.plot(speeds, requests, "b")
    plot.scatter(speeds, requests, ranges, c="r", marker="o")
    plot.ylabel("Average requests per second")
    plot.xlabel("Speed")
    plot.show()
"""